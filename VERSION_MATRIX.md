# Version Matrix

| act                |        1.6.x |        1.7.x |       1.8.7 |   1.8.8-RC7 |
| ---                |        ----: |        ----: |       ----: |       ----: |
| aaa                |        1.3.x |        1.4.0 |       1.4.2 |       1.4.3 |
| beetl              |        1.2.x |        1.3.x |       1.4.1 |       1.4.2 |
| beetlsql           |        1.3.x |        1.4.3 |       1.4.5 |       1.4.6 |
| e2e                |              |              |             |       0.0.4 |
| ebean-java7        |        1.3.x |        1.4.x |       1.5.3 |       1.6.2 |
| ebean              | 1.2.x, 1.3.x |        1.4.x |       1.6.2 |       1.6.5 |
| eclipselink(java8) |          N/A | 1.0.x, 1.1.x |       1.2.3 |       1.3.0 |
| excel              |        1.3.x |        1.4.x |       1.4.3 |       1.4.4 |
| freemarker         |        1.2.x |        1.3.0 |       1.3.2 |       1.3.3 |
| hibernate          |          N/A | 1.0.x, 1.1.x |       1.2.3 |       1.3.0 |
| jax-rs(java8)      |          N/A |        1.0.x |       1.0.2 |       1.0.3 |
| jpa-common         |          N/A | 1.0.x, 1.1.x |       1.2.3 |       1.3.0 |
| morphia            |        1.3.x |        1.4.0 |       1.4.2 |       1.5.1 |
| mustache           |        1.3.x |        1.4.0 |       1.4.2 |       1.4.3 |
| social             |       0.11.x |       0.12.0 |      0.12.2 |      0.12.3 |
| sql-common         |        1.2.x |        1.3.1 |       1.3.3 |       1.3.4 |
| storage            |       0.12.x |       0.13.0 |      0.13.2 |      0.13.3 |
| thymeleaf          |        1.2.x |        1.3.0 |       1.3.2 |       1.3.3 |
| velocity           |        1.2.x |        1.3.0 |       1.3.2 |       1.3.3 |

## Note

* act-ebean renamed to act-ebean-java7 since 1.5.2
* act-ebean2 renamed to act-ebean since 1.6.0
